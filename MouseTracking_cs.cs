using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// マウスをトラッキングするスクリプト。
/// 一応これ貼ったらその方向を見てくれます。
/// </summary>
public class MouseTracking_cs : MonoBehaviour
{
    void Update()
    {
        MouseTrackingFunc();
    }

    public void MouseTrackingFunc()
    {
        // プレイヤーのスクリーン座標を計算する
        var screenPos = Camera.main.WorldToScreenPoint(transform.position);

        // プレイヤーから見たマウスカーソルの方向を計算する
        var direction = Input.mousePosition - screenPos;

        // マウスカーソルが存在する方向の角度を取得する
        var angle = GetAngle(Vector3.zero, direction);

        // プレイヤーがマウスカーソルの方向を見るようにする
        var angles = transform.localEulerAngles;
        angles.z = angle - 90;
        transform.localEulerAngles = angles;
    }

    float GetAngle(Vector2 from, Vector2 to)
    {
        var dx = to.x - from.x;
        var dy = to.y - from.y;
        var rad = Mathf.Atan2(dy, dx);
        return rad * Mathf.Rad2Deg;
    }

}
