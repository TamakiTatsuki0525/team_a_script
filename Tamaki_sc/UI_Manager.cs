using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //UI機能を扱うときに追記する
/// <summary>
/// UIや点数を管理するスクリプト。
/// </summary>
public class UI_Manager : MonoBehaviour
{
    //public
    public SceneManagerCS _SceneManagerCS;
    public Text ScoreText;
    public Text NormalBulletText;
    public Text EraseBulletText;
    public GunCS _GunCS;

    [System.NonSerialized] public int Score;

    // Start is called before the first frame update
    void Start()
    {
        //ScoreText.text = "Score:" + Score.ToString(); //ScoreTextの文字をScore:Scoreの値にする

        ScoreText.text = "Score:" + Score.ToString();
        NormalBulletText.text = "通常弾:" + _GunCS.NormalBulletNum.ToString();
        EraseBulletText.text = "消滅弾:" + _GunCS.EraseBulletNum.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Score);
        ScoreText.text = "Score:" + Score.ToString();
        NormalBulletText.text = "通常弾:" + _GunCS.NormalBulletNum.ToString();
        EraseBulletText.text = "消滅弾:" + _GunCS.EraseBulletNum.ToString();
        _SceneManagerCS.GetScore = Score;
    }
}
