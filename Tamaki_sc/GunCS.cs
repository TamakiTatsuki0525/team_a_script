using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// eðÅ¿o·XNvg
/// </summary>
public class GunCS : MonoBehaviour
{
    //public
    public GameObject Normal_Bullet;
    public GameObject Erase_Bullet;
    public GameObject Center_Object;

    public int NormalBulletNum = 4;
    public int EraseBulletNum = 1;
    public float _period = 2.0f;
    public float RotateMultiQua = 1.0f;

    //private
    GameObject SoundManagerOB;
    SoundManagerCS soundManager;

    int GunState = 0;
    float NowRotateQua = 0;
    
    //è
    //eÌeÛÌóÔ
    private readonly int Normal = 1;
    private readonly int Erase = 2;

    // Start is called before the first frame update
    void Start()
    {
        GunState = Normal;
        SoundManagerOB = GameObject.FindGameObjectWithTag("SoundManager");
        soundManager = SoundManagerOB.GetComponent<SoundManagerCS>();
    }

    // Update is called once per frame
    void Update()
    {
        GunInputManager();
    }
    //eð­Ë·é¨
    void FireBulletFunc()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (GunState == Normal  && NormalBulletNum > 0)
            {
                Debug.Log("ÊÌe­Ë");
                Instantiate(Normal_Bullet,this.transform.position,this.transform.rotation);
                NormalBulletNum--;
                soundManager.SEManagerFunc(soundManager.ShotSE);
            }

            else if (GunState == Erase && EraseBulletNum > 0)
            {
                Debug.Log("Áêe");
                Instantiate(Erase_Bullet, this.transform.position, this.transform.rotation);
                EraseBulletNum--;
                soundManager.SEManagerFunc(soundManager.ShotSE);
            }
            
        }

    }

    void GunInputManager()
    {
        //ÈºeÌíÞÌÏX
        float InputMouseWheelQua = Input.GetAxis("Mouse ScrollWheel");

        //ãüÍ
        if (InputMouseWheelQua > 0)
        {
            GunState = Normal;
        }
        //ºüÍ
        else if (InputMouseWheelQua < 0)
        {
            GunState = Erase;
        }

        //Èºpx²®
        //¶Éñ]
        if (Input.GetKey(KeyCode.A))
        {
            //NowRotateQua++;
            NowRotateQua += RotateMultiQua;
        }
        //EÉñ]
        else if (Input.GetKey(KeyCode.D))
        {
            //NowRotateQua--;
            NowRotateQua -= RotateMultiQua;
        }

        // S_centerÌüèðA²axisÅAperiodüúÅ~^®
        transform.RotateAround(
            Center_Object.transform.position,
            new Vector3(0, 0, 1),
            360 / _period * Time.deltaTime
        );

        Vector3 diff = (Center_Object.transform.position - this.transform.position).normalized;
        this.transform.rotation = Quaternion.FromToRotation(Vector3.up, diff);

        transform.eulerAngles += new Vector3(0, 0, NowRotateQua);

        Debug.Log("eÌeÌóÔ" + GunState);
        FireBulletFunc();
    }

}
