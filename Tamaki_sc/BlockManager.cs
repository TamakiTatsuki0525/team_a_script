using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ブロックの消滅、連結を外す等の
/// ブロックを管理するスクリプト
/// </summary>
public class BlockManager : MonoBehaviour
{
    //public
    public Sprite DamageSprite;

    public bool IsBulletHit = false; //後から消す。
    public float MoveStopTime = 1.0f; //ブロックが弾に当たってから止まるまでの時間
    public float MoveMultiParameter = 2.0f;

    //private
    GameObject GetBullet;
    Rigidbody2D _BulletRigid;
    Rigidbody2D _rigid;
    SpriteRenderer _SpriteRender;
    GameObject SoundManagerOB;
    SoundManagerCS soundManagerCS;
    TmpBulletCS _TmpBulletCS;
    GameObject UI_ManagerOB;
    UI_Manager UI_ManagerCS;
    GameObject GameSystemManagerOB;
    GameSystemManager GameSystemManagerCS;

    bool IsDestory = false;
    bool IsDisConnect = false;
    bool IsPhaseFinish = false;
    bool IsOthersBlockConnect = false;
    float _time;
    float _time2;

    void Start()
    {
        SoundManagerOB = GameObject.FindGameObjectWithTag("SoundManager");
        soundManagerCS = SoundManagerOB.GetComponent<SoundManagerCS>();
        UI_ManagerOB = GameObject.FindGameObjectWithTag("UI_Manager");
        UI_ManagerCS = UI_ManagerOB.GetComponent<UI_Manager>();
        GameSystemManagerOB = GameObject.FindGameObjectWithTag("GameSystem");
        GameSystemManagerCS = GameSystemManagerOB.GetComponent<GameSystemManager>();

        _rigid = GetComponent<Rigidbody2D>();
        _SpriteRender = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (IsPhaseFinish)
        {
            _time += Time.deltaTime;
            if (_time >= MoveStopTime)
            {
                _rigid.velocity = new Vector2(0, 0);
            }
        }

        if (IsOthersBlockConnect)
        {
            _time2 += Time.deltaTime;
            if (_time2 >= MoveStopTime)
            {
                _rigid.velocity = new Vector2(0, 0); 
            }
        }

       
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("なんかに当たった");
        //普通の弾
        if (collision.gameObject.tag == "Bullet")
        {
            if (!IsDisConnect)
            {
                Debug.Log("弾当たった");

                GetBullet = collision.gameObject;
                _BulletRigid = GetBullet.GetComponent<Rigidbody2D>();
                Debug.Log("取得した速さ" + _BulletRigid.velocity);

                _rigid.velocity = _BulletRigid.velocity * MoveMultiParameter;


                Destroy(GetBullet);
                GameSystemManagerCS.DestryBulletNum++;

                IsPhaseFinish = true;

                _SpriteRender.sprite = DamageSprite;
                IsDisConnect = true;
                IsOthersBlockConnect = false;
                UI_ManagerCS.Score += 100;
                soundManagerCS.SEManagerFunc(soundManagerCS.EnemyMoveSE);
                GameSystemManagerCS.ClearNum++;
                Debug.Log("連結外れる");
            }
            else
            {
                GetBullet = collision.gameObject;
                Destroy(GetBullet);
                GameSystemManagerCS.DestryBulletNum++;
                UI_ManagerCS.Score -= 50;
            }
        }

        //消滅弾
        else if (collision.gameObject.tag == "Erase_Bullet")
        {
            if (!IsDisConnect)
            {
                //IsDestory = true;
                GetBullet = collision.gameObject;
                _TmpBulletCS = GetBullet.GetComponent<TmpBulletCS>();
                UI_ManagerCS.Score += 150;
                GameSystemManagerCS.ClearNum++;
                if (_TmpBulletCS.PirceNum >= 1 && _TmpBulletCS.AfterPirce <= 1)
                {
                    _TmpBulletCS.PirceNum--;
                    _TmpBulletCS.AfterPirce++;
                }
                else
                {
                    Destroy(GetBullet);
                    GameSystemManagerCS.DestryBulletNum++;
                    Debug.Log("破壊する");
                }
                soundManagerCS.SEManagerFunc(soundManagerCS.DestroySE);
                Destroy(this.gameObject);
            }
            else
            {
                GetBullet = collision.gameObject;
                _TmpBulletCS = GetBullet.GetComponent<TmpBulletCS>();
                if (_TmpBulletCS.PirceNum >= 1 && _TmpBulletCS.AfterPirce <= 1)
                {
                    _TmpBulletCS.PirceNum--;
                    _TmpBulletCS.AfterPirce++;
                }
                else
                {
                    Destroy(GetBullet);
                    GameSystemManagerCS.DestryBulletNum++;
                    Debug.Log("破壊する");
                }
                UI_ManagerCS.Score -= 50;
                soundManagerCS.SEManagerFunc(soundManagerCS.DestroySE);
                Destroy(this.gameObject);
            }
        }

        else if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("敵同士");
            IsOthersBlockConnect = true;
            GameObject GetObject = collision.gameObject;
            Rigidbody2D _GetRigid = GetObject.GetComponent<Rigidbody2D>();
            Debug.Log(_GetRigid.velocity);
            var _vel = _GetRigid.velocity * MoveMultiParameter;
            Debug.Log(_vel);
            _rigid.velocity = _vel;
        }

    }
}
