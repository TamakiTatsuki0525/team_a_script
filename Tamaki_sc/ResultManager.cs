using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultManager : MonoBehaviour
{
    //public
    public GameObject ClearText;
    public GameObject GameOverText;
    public Text ScoreText;

    //private
    GameObject SceneManagerOB;
    SceneManagerCS _SceneManagerCS;

    // Start is called before the first frame update
    void Start()
    {
        SceneManagerOB = GameObject.Find("SceneManagerObject");
        _SceneManagerCS = SceneManagerOB.GetComponent<SceneManagerCS>();
        ScoreText.text =  _SceneManagerCS.GetScore.ToString() + "�_�I";
        if (_SceneManagerCS.IsClear)
        {
            ClearText.SetActive(true);
        }
        else
        {
            GameOverText.SetActive(true);
        }
        Debug.Log(_SceneManagerCS.GetScore);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
