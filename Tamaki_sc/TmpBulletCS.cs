using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 弾を動かす仮のスクリプト
/// </summary>
public class TmpBulletCS : MonoBehaviour
{
    //public
    public float Speed;
    public bool IsEraseBullet = false;
    [System.NonSerialized] public int PirceNum = 0;
    [System.NonSerialized] public int AfterPirce = 0;

    //private
    Rigidbody2D _rigid;
    Vector2 _StoreVel;
    GameObject SoundManagerOB;
    SoundManagerCS soundManager;
    GameObject UI_ManagerOB;
    UI_Manager UI_ManagerCS;

    int ReflectNum = 0;
    
    //定数
    private readonly int WallX = 1;
    private readonly int WallY = 2;

    void Start()
    {
        SoundManagerOB = GameObject.FindGameObjectWithTag("SoundManager");
        soundManager = SoundManagerOB.GetComponent<SoundManagerCS>();
        UI_ManagerOB = GameObject.FindGameObjectWithTag("UI_Manager");
        UI_ManagerCS = UI_ManagerOB.GetComponent<UI_Manager>();

        _rigid = GetComponent<Rigidbody2D>();
        _rigid.velocity = transform.up * Speed;//速さを代入
        _StoreVel = _rigid.velocity;
    }

    private void Update()
    {
        Debug.Log(AfterPirce);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (IsEraseBullet &&(collision.gameObject.tag == "XRoop" || collision.gameObject.tag == "YRoop"))
        {
            ReflectNum++;
            Debug.Log(ReflectNum + "  反射した回数");
            if (ReflectNum >= 2 && PirceNum <= 2)
            {
                PirceNum++;
                ReflectNum = 0;
                soundManager.SEManagerFunc(soundManager.BulletReflectSE2);
            }
            else
            {
                soundManager.SEManagerFunc(soundManager.BulletReflectSE);
            }
            UI_ManagerCS.Score += 50;
        }

        if (collision.gameObject.tag == "XRoop")
        {
            _rigid.velocity = new Vector2(_StoreVel.x, -_StoreVel.y);
            _StoreVel = _rigid.velocity;

            Vector3 _vec = new Vector3(transform.position.x + _StoreVel.x, transform.position.y + _StoreVel.y, 0);
            Vector3 diff = (_vec - this.transform.position).normalized;
            this.transform.rotation = Quaternion.FromToRotation(Vector3.up, diff);

            soundManager.SEManagerFunc(soundManager.BulletReflectSE);

            //Debug.Log(_rigid.velocity);
            Debug.Log("横の壁");
            UI_ManagerCS.Score += 50;
        }

        else if (collision.gameObject.tag == "YRoop")
        {
            _rigid.velocity = new Vector2(-_StoreVel.x,_StoreVel.y);
            _StoreVel = _rigid.velocity;

            Vector3 _vec = new Vector3(transform.position.x + _StoreVel.x, transform.position.y + _StoreVel.y, 0);
            Vector3 diff = (_vec - this.transform.position).normalized;
            this.transform.rotation = Quaternion.FromToRotation(Vector3.up, diff);

            soundManager.SEManagerFunc(soundManager.BulletReflectSE);

            //Debug.Log(_rigid.velocity);
            Debug.Log("縦の壁");
            UI_ManagerCS.Score += 50;
        }

        else if (collision.gameObject.tag == "Enemy")
        {
            _rigid.velocity = _StoreVel;
        }
    }
}
