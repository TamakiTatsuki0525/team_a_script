using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// マウスカーソルが刺しているところにオブジェクトを移動させるスクリプト
/// </summary>
public class Mouse_Chaising : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var targetPos = UnityEngine.Camera.main.ScreenToWorldPoint(Input.mousePosition);

        targetPos += new Vector3(0,0,10);

        this.transform.position = targetPos;
    }

}
