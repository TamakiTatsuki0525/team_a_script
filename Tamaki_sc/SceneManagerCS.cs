using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/// <summary>
/// シーンを管理するスクリプト
/// </summary>
public class SceneManagerCS : MonoBehaviour
{
    //public
    public bool IsClear = false;
    public int GetScore;

    //定数
    public readonly int GotoResult = 1;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);//破壊不能
        Application.targetFrameRate = 60;//フレームレートを６０に
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            //エディタ側
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            //実行ファイル
#else
    Application.Quit();
#endif
        }
    }

    //もらう引数によってどのシーンに移動するか変わる。
    public void SceneTransition(int ArgNum)
    {
        if (ArgNum == GotoResult)
        {
            SceneManager.LoadScene("Result");
            
        }
        else
        {
            Debug.Log("シーン遷移エラー");
        }
    }

}
