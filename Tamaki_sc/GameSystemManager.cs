using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ゲームシステムを管理するスクリプト
/// </summary>
public class GameSystemManager : MonoBehaviour
{
    //public 
    public SceneManagerCS _SceneMangerCS;
    public GunCS _GunCS;

    public int ClearNum = 0;
    public int DestryBulletNum = 0;

    //private
    int GameOverNeedNum = 0;

    // Start is called before the first frame update
    void Start()
    {
        GameOverNeedNum = _GunCS.NormalBulletNum + _GunCS.EraseBulletNum;
        Debug.Log("ta" + GameOverNeedNum);
    }

    // Update is called once per frame
    void Update()
    {
        SystemManagerFunc();
        Debug.Log(DestryBulletNum + "使った弾丸の数");
        
    }

    void SystemManagerFunc()
    {

        if (ClearNum == 6)
        {
            _SceneMangerCS.IsClear = true;
            _SceneMangerCS.SceneTransition(_SceneMangerCS.GotoResult);
        }

        else if (DestryBulletNum == GameOverNeedNum)
        {
            _SceneMangerCS.IsClear = false;
            _SceneMangerCS.SceneTransition(_SceneMangerCS.GotoResult);
        }
    }

}
