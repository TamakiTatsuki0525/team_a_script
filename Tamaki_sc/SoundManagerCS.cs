using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// サウンド関係を管理するスクリプト
/// 鳴らすの事態は他スクリプトからの呼び出し
/// </summary>
public class SoundManagerCS : MonoBehaviour
{
    //public
    public AudioClip[] SEList = new AudioClip[5];
    public AudioSource _AS;

    //定数
    public readonly int DestroySE = 1;
    public readonly int ShotSE = 2;
    public readonly int EnemyMoveSE = 3;
    public readonly int BulletReflectSE = 4;
    public readonly int BulletReflectSE2 = 5;

    public void SEManagerFunc(int InsSENum)
    {
        _AS.PlayOneShot(SEList[InsSENum - 1]);
    }

}
